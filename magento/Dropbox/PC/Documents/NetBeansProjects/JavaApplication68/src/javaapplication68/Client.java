/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication68;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;

/**
 *
 * @author Ngoc Long
 */
public class Client {
    public static void main(String[] args) throws RemoteException, NotBoundException {
        Registry rg = LocateRegistry.getRegistry(1102);
        RMI rmi = (RMI) rg.lookup("long");
        Scanner sc = new Scanner(System.in);
        System.out.println("Moi ban nhap so : ");
        int a = sc.nextInt();
        System.out.println("" + rmi.SNT(a));
    }
}
