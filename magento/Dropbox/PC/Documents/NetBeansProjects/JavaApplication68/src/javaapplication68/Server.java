/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication68;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 *
 * @author Ngoc Long
 */
public class Server extends UnicastRemoteObject implements RMI {

    public Server() throws RemoteException {
        Registry rg = LocateRegistry.createRegistry(1102);
        rg.rebind("long", this);
    }

    public static void main(String[] args) throws RemoteException {
        Server sv = new Server();
    }

    @Override
    public String SNT(int a) throws RemoteException {
        String s = null;
        if (a < 2) {

            s = "day khong phai so nguyen to";
        }
        for (int i = 0; i < Math.sqrt(a); i++) {
            if (a % i == 0) {
                s = "day khong phai so nguyen to";
            } else {
                s = "day la so nguyen to";
            }
        }
        return s;
    }

}
