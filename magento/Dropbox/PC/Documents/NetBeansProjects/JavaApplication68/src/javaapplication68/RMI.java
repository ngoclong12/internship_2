/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication68;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Ngoc Long
 */
public interface RMI extends Remote {

    public String SNT(int a) throws RemoteException;
}
