/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication66;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 *
 * @author Ngoc Long
 */
public class Client {

    DatagramSocket sk;
    DatagramPacket pk;

    public void guiDL(String s) throws UnknownHostException, IOException {
        byte[] data = s.getBytes();
        InetAddress ip = InetAddress.getByName("localhost");
        pk = new DatagramPacket(data, data.length, ip, 6789);
        sk.send(pk);
    }

    public String nhanDL() throws IOException {
        byte[] data = new byte[1024];
        pk = new DatagramPacket(data, data.length);
        sk.receive(pk);
        String s = new String(pk.getData()).trim();
        return s;
    }

    public static void main(String[] args) throws SocketException, IOException {
        Client cl = new Client();
        cl.sk = new DatagramSocket(1102);
        Scanner sc = new Scanner(System.in);
        System.out.println("Moi ban nhap so tu '0-9' : ");
        String a = sc.nextLine();
        cl.guiDL(a);
        System.out.println("Tra ve : " + cl.nhanDL());
    }
}
