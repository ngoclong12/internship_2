/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication66;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 *
 * @author Ngoc Long
 */
public class Server {
    DatagramSocket sk;
    DatagramPacket pk;

    public void guiDL(String s) throws UnknownHostException, IOException {
        byte[] data = s.getBytes();
        InetAddress ip = InetAddress.getByName("localhost");
        pk = new DatagramPacket(data, data.length, ip, 1102);
        sk.send(pk);
    }

    public String nhanDL() throws IOException {
        byte[] data = new byte[1024];
        pk = new DatagramPacket(data, data.length);
        sk.receive(pk);
        String s = new String(pk.getData()).trim();
        return s;
    }
    
    public static void main(String[] args) throws SocketException, IOException {
        Server sv = new Server();
        sv.sk = new DatagramSocket(6789);
        int a = Integer.parseInt(sv.nhanDL());
        String kq = "";
        if(a == 1){
            kq = "mot";
        }
        else if(a == 2){
            kq = "hai";
        }
        else if(a == 3){
            kq = "ba";
        }
        else if(a == 4){
            kq = "bon";
        }
        else if(a == 5){
            kq = "nam";
        }
        else if(a == 6){
            kq = "sau";
        }
        else if(a == 7){
            kq = "bay";
        }
        else if(a == 8){
            kq = "tam";
        }
        else if(a == 9){
            kq = "chin";
        }else{
            kq= "Eror..^^\n vui long thu lai!";
        }
        sv.guiDL(kq);
       
    }
}
