/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de7_cau1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 *
 * @author Ngoc Long
 */
public class Client {
    DatagramSocket sk;
    DatagramPacket pk;
    
    public void connect () throws SocketException{
        sk = new DatagramSocket(1102);
    }
    
    public void guiDL(String s) throws UnknownHostException, IOException{
        byte[] data = s.getBytes();
        InetAddress ipsv = InetAddress.getByName("localhost");
        pk = new DatagramPacket(data, data.length, ipsv, 6789);
        sk.send(pk);
        
    }
    
    public String nhanDL() throws IOException{
        byte[] data = new byte[1024];
        pk = new DatagramPacket(data, data.length);
        sk.receive(pk);
        String s = new String(pk.getData()).trim();
        return s;
    }
    
    public static void main(String[] args) throws SocketException, IOException {
        Client cl = new Client();
        cl.connect();
        Scanner sc = new Scanner(System.in);
        System.out.println("Nhap so :");
        String a = sc.nextLine();
        cl.guiDL(a);
        System.out.println("in ra : "+cl.nhanDL());
    }
}
