/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de7_cau1;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 *
 * @author Ngoc Long
 */
public class Server {

    DatagramSocket sk;
    DatagramPacket pk;

    public void connect() throws SocketException {
        sk = new DatagramSocket(6789);
    }

    public String nhanDL() throws IOException {
        byte[] data = new byte[1024];
        pk = new DatagramPacket(data, data.length);
        sk.receive(pk);
        String s = new String(pk.getData()).trim();
        return s;
    }

    public void guiDL(String s) throws UnknownHostException, IOException {
        byte[] data = s.getBytes();
        InetAddress ipcl = InetAddress.getByName("localhost");
        pk = new DatagramPacket(data, data.length, ipcl, 1102);
        sk.send(pk);
    }

    public static void main(String[] args) throws SocketException, IOException {
        Server sv = new Server();
        sv.connect();
        String kq = null;
        int a = Integer.parseInt(sv.nhanDL());

        if (a == 1) {
            kq = "mot";
        } else if (a == 2) {
            kq = "hai";
        } else if (a == 3) {
            kq = "ba";
        } else if (a == 4) {
            kq = "bon";
        } else if (a == 5) {
            kq = "nam";
        } else if (a == 6) {
            kq = "sau";
        } else if (a == 7) {
            kq = "bay";
        } else if (a == 8) {
            kq = "tam";
        } else if (a == 9) {
            kq = "chin";
        } else{
            kq = "*Eror...";
        }

        sv.guiDL(kq);
    }
}
