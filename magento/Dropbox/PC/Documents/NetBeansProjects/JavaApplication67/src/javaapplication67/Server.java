/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication67;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Ngoc Long
 */
public class Server {
    
    public static void main(String[] args) throws IOException {
        ServerSocket svsk = new ServerSocket(1102);
        Socket sk = svsk.accept();
        DataInputStream in = new DataInputStream(sk.getInputStream());
        DataOutputStream out = new DataOutputStream(sk.getOutputStream());
        int a = in.readInt();
        int kq = 1;
        for(int i = 1;i<=a;i++){
            kq = kq*i;
        }
        out.writeUTF("" + kq);
    }
}
