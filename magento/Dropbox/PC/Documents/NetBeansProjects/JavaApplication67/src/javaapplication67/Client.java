/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication67;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author Ngoc Long
 */
public class Client {
    public static void main(String[] args) throws IOException {
        Socket sk = new Socket("localhost", 1102);
        DataInputStream in = new DataInputStream(sk.getInputStream());
        DataOutputStream out = new DataOutputStream(sk.getOutputStream());
        Scanner sc = new Scanner(System.in);
        System.out.println("Moi ban nhap so nguyen : ");
        int a = sc.nextInt();
        out.writeInt(a);
        System.out.println("Co giai thua la : " + in.readUTF());
    }
}
