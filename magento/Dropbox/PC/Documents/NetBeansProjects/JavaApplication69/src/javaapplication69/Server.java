/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication69;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Ngoc Long
 */
public class Server {
    public static int UCLN(int a, int b){
        if (a == 0 || b == 0) {
            return a+b;
        }
        while (a != b) {            
            if (a > b) {
                a -= b;
            }else{
                b -= a;
            }
        }        
        return a;
    }
    public static void main(String[] args) throws IOException {
        ServerSocket svsk = new ServerSocket(1102);
        Socket sk = svsk.accept();
        DataInputStream in = new DataInputStream(sk.getInputStream());
        DataOutputStream out = new DataOutputStream(sk.getOutputStream());
        int a = in.readInt();
        System.out.println("so a : " + a);
        int b = in.readInt();
        System.out.println("so b : " + b);
        out.writeInt(a*b/UCLN(a, b));
    }
}
