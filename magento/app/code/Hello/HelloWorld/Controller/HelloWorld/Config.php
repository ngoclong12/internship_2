<?php
namespace Hello\HelloWorld\Controller\HelloWorld;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Result\PageFactory;
use Hello\HelloWorld\Helper\Data;

class Config extends Action
{

    protected Data $helper;
    protected PageFactory $pageFactory;
    private \Magento\Framework\Controller\Result\ForwardFactory $_forwardFactory;

    public function __construct(Context $context, Data $helper, PageFactory $pageFactory, RequestInterface $request, \Magento\Framework\Controller\Result\ForwardFactory $forwardFactory)
    {
        $this->helper = $helper;
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->_forwardFactory = $forwardFactory;
    }


    public function execute()
    {

        $stt = $this->helper->getGeneralConfig('status');
        $layout = $this->pageFactory->create();
        if ($stt == 0){
            $layout = $this->_forwardFactory->create();
            $layout->setController('index');
            $layout->forward('defaultNoRoute');
        }
        return $layout;

    }
}
