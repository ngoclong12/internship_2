<?php

namespace Hello\HelloWorld\Controller\HelloWorld;

use Magento\Framework\App\Action\Action;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\JsonFactory;

class Returnjson extends Action
{
    protected PageFactory $pageFactory;
    protected RequestInterface $request;
    protected JsonFactory $resultJsonFactory;

    public function __construct(Context $context, PageFactory $pageFactory, RequestInterface $request, JsonFactory $resultJson)
    {
        
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->request = $request;
        $this->resultJsonFactory = $resultJson;
    }


    public function execute()
    {
        $name = $this->getRequest()->getParam('name', 'Bss Group');
        $dob = $this->getRequest()->getParam('dob', '21-12-2012');
        $address = $this->getRequest()->getParam('address','48-ToHuu');
        $arr = [
            'name' => $name,
            'dob' => $dob,
            'address' => $address
        ];

        $result = $this->resultJsonFactory->create();
        $result->setData($arr);
        return $result;
    }
}
