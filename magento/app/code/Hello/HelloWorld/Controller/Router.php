<?php
declare(strict_types=1);

namespace Hello\HelloWorld\Controller;

use Magento\Framework\App\Action\Forward;
use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\RouterInterface;

/**
 * Class Router
 */
class Router implements RouterInterface
{
    /**
     * @var ActionFactory
     */
    private ActionFactory $actionFactory;
    protected RequestInterface $Request;

    /**
     * Router constructor.
     *
     * @param ActionFactory $actionFactory
     * @param RequestInterface $request
     */
    public function __construct(ActionFactory $actionFactory, RequestInterface $request)
    {
        $this->actionFactory = $actionFactory;
        $this->Request = $request;
    }

    /**
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request): ?ActionInterface
    {
        $identifier = trim($request->getPathInfo(), '/');
        $array = explode('/', $identifier);
        if (isset($array) && $array[0] == 'internship-course') {
            if (count($array) <= 2) {
                $request->setModuleName('routing');
                $request->setControllerName('index');
                $request->setActionName('index');
                if (count($array) == 2) {
                    $request->setParams([
                        'name' => $array[1]
                    ]);
                }
                return $this->actionFactory->create(Forward::class, ['request' => $request]);
            }
        }
        return null;
    }
}
